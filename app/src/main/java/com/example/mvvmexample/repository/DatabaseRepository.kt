package com.example.mvvmexample.repository

import android.content.Context
import android.os.Bundle

class DatabaseRepository() {
    fun querySomething(): List<String> {
        return listOf()
    }

    companion object {
        fun newInstance(context: Context): DatabaseRepository{
            val args = Bundle()
            val instance = DatabaseRepository()
            return instance
        }
    }
}