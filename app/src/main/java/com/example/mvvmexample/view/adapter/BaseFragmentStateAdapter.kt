package com.example.mvvmexample.view.adapter

import android.util.Log
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.fragment.app.FragmentManager
import androidx.lifecycle.Lifecycle
import androidx.viewpager.widget.PagerAdapter
import androidx.viewpager2.adapter.FragmentStateAdapter

open class BaseFragmentStateAdapter<T>(fragmentMangager: FragmentManager, lifecycle: Lifecycle) :
    FragmentStateAdapter(fragmentMangager, lifecycle) {
    val listFragment = mutableListOf<T>()
    private val listTitle = mutableListOf<String>()
    private val listId = mutableListOf<Long>()

    fun addFragment(id: Long, fragment: T, title: String) {
        listId.add(id)
        listFragment.add(fragment)
        listTitle.add(title)
    }

    fun addFragment(fragment: T, id: Long) {
        addFragment(id, fragment, "")
    }

    fun clearFragment(){
        listId.clear()
        listFragment.clear()
        listTitle.clear()

    }


    fun removeFragmentAt(position: Int) {
        listFragment.removeAt(position)
        listTitle.removeAt(position)
        listId.removeAt(position)
        notifyDataSetChanged()
    }

    fun getTitle(position: Int): String {
        return listTitle[position]
    }

    override fun getItemCount(): Int {
        return listId.size
    }

    override fun getItemId(position: Int): Long {
        return listId[position]
    }

    override fun containsItem(itemId: Long): Boolean {
        return listId.contains(itemId)
    }

    override fun createFragment(position: Int): Fragment {
        return listFragment[position] as Fragment
    }



    fun getChildAt(position: Int): T? {
        return if(listFragment.size > position){
            listFragment[position]
        } else {
            null
        }
    }
}