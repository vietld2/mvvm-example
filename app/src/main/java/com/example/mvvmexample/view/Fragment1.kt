package com.example.mvvmexample.view

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ListView
import androidx.annotation.LayoutRes
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import com.example.mvvmexample.R
import com.example.mvvmexample.view.adapter.CountryAdapter
import com.example.mvvmexample.viewmodel.MainViewModel
import com.example.trainingground.model.Country

class Fragment1(@LayoutRes layoutId: Int) : Fragment(layoutId) {
    private lateinit var countryAdapter: CountryAdapter

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initView(view)

        val viewModel = ViewModelProvider(requireActivity())
            .get(MainViewModel::class.java)


        viewModel.countryLiveData.observe(requireActivity()) {
            countryAdapter.submitList(it)
            countryAdapter.notifyDataSetChanged()
        }
    }

    private var rvCountry: ListView? = null
    private fun initView(view: View) {
        rvCountry = view.findViewById(R.id.rv_country_1)
        countryAdapter = CountryAdapter(requireContext())
        rvCountry?.adapter = countryAdapter
    }
}