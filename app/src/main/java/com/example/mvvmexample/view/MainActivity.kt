package com.example.mvvmexample.view

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.Button
import android.widget.EditText
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.viewpager2.widget.ViewPager2
import com.example.mvvmexample.R
import com.example.mvvmexample.view.adapter.BaseFragmentStateAdapter
import com.example.mvvmexample.viewmodel.MainViewModel
import com.example.trainingground.model.Country

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        initView()

        val mainViewModel = ViewModelProvider(this).get(MainViewModel::class.java)


        btnAdd?.setOnClickListener {
            hideKeyboard()
            val country = Country(
                edCountryName?.text.toString(),
                edCapital?.text.toString()
            )
            mainViewModel.addCountry(country)
        }

        btnFilter?.setOnClickListener {
            hideKeyboard()
            mainViewModel.validateCountry()
        }
    }

    private var edCountryName: EditText? = null
    private var edCapital: EditText? = null
    private var btnAdd: Button? = null
    private var btnFilter: Button? = null

    private fun initView() {
        edCountryName = findViewById(R.id.ed_countryName)
        edCapital = findViewById(R.id.ed_capital)
        btnAdd = findViewById(R.id.btn_add)
        btnFilter = findViewById(R.id.btn_filter)

        val viewPager = findViewById<ViewPager2>(R.id.vp_main)
        viewPager.offscreenPageLimit = 2
        val fragmentAdapter = BaseFragmentStateAdapter<Fragment>(supportFragmentManager, lifecycle)
        fragmentAdapter.addFragment(Fragment1(R.layout.fragment_1), 0)
        fragmentAdapter.addFragment(Fragment2(R.layout.fragment_2), 1)
        viewPager.adapter = fragmentAdapter
    }

    fun hideKeyboard() {
        val imm: InputMethodManager =
            getSystemService(INPUT_METHOD_SERVICE) as InputMethodManager
        var view: View? = currentFocus
        if (view == null) {
            view = View(this)
        }
        imm.hideSoftInputFromWindow(view.windowToken, 0)
    }
}