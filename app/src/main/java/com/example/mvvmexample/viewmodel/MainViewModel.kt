package com.example.mvvmexample.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.trainingground.model.Country
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class MainViewModel : ViewModel() {
    val countryLiveData: MutableLiveData<List<Country>>
            by lazy {
                MutableLiveData<List<Country>>()
            }

    private var currentData = mutableListOf<Country>()

    fun addCountry(country: Country) {
        currentData.add(country)
        countryLiveData.value = (currentData)

    }

    fun validateCountry() {
        viewModelScope.launch(Dispatchers.Default) {
            val currentData = currentData.filter {
                it.capital.isNotEmpty() && it.name.isNotEmpty()
            }

            countryLiveData.postValue(currentData)
        }
    }
}